package ictgradschool.industry.lab05.ex02;

/**
 * Main program for Exercise Two.
 */
public class ExerciseTwo {

    public void start() {

        Animal[] animals = new Animal[3];

        // TODO Populate the animals array with a Bird, a Dog and a Horse.

        animals[0] = new Bird();
        animals[1] = new Dog();
        animals[2] = new Horse();

        processAnimalDetails(animals);

    }

    private void processAnimalDetails(Animal[] list) {
        // TODO Loop through all the animals in the given list, and print their details as shown in the lab handout.

        for (int i = 0; i < list.length; i++) {
            System.out.println(list[i].myName() + "says " + list[i].sayHello());
                if (list[i].isMammal()) {
                    System.out.println(list[i].myName() + "is a mammal");
                }
                else {
                    System.out.println(list[i].myName() + "is a non-mammal");
                }
           // System.out.println(list[i].myName() + "is a " + list[i].isMammal());
//            list[i].myName();
//            System.out.println( list[i].myName() + list[i].sayHello());
           // list[i].legCount();
            System.out.println("Did I forget to tell you that I have " + list[i].legCount() + " legs");
               if (list[i] instanceof IFamous) {
                    IFamous other = (IFamous) list[i];
                    System.out.println(other.famous());
                }
                // TODO If the animal also implements IFamous, print out that corresponding info too.
            System.out.println("---------------------------------------------------");
        }

    }


    public static void main(String[] args) {
        new ExerciseTwo().start();
    }
}
