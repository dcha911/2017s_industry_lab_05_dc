package ictgradschool.industry.lab05.ex02;

/**
 * Created by dcha911 on 21/11/2017.
 */
public abstract class Animal {

        /**
         * Returns a string containing the greeting
         */
        public abstract String sayHello();

        /**
         * Returns true or false
         */
        public boolean isMammal() {
            return true;
        };

        /**
         * Returns the name, followed by “the” followed by the animal type, e.g. “George the Monkey”
         */
        public abstract String myName();

        /**
         * Returns the number of legs
         */
        public int legCount() {
            return 0;
        }

}
